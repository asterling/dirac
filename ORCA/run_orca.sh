#!/bin/bash
#
#$ -cwd
#
#$ -pe smp 2
#$ -l s_rt=360:00:00
#
module load openmpi2
setenv path "$path\:/opt/openmpi2/bin"
setenv ld_library_path "/opt/openmpi2/lib"
setenv ORIG $PWD
setenv SCR $TMPDIR
# Make the scratch directroy
mkdir -p $SCR
# Copy the ORCA input (.inp) file to the node
cp orca_test.inp $SCR
#
cd $SCR
# Run ORCA with the full path to the binary, outputting to orca_test.out
/usr/local/orca_4_0_1_2_linux_x86-64/orca $SCR/orca_test.inp >> $ORIG/orca_test.out
# Remove all the tempoary files. ORCA should do this if the run finishes
rm -f *.tmp
# Copy only the .out and .xyz files back to ORIG
cp *.out *.xyz $ORIG
cd /
# Remove all the files on the node
rm -Rf $SCR
