# Gaussian
Using Gaussian on dirac

Pete has generated scripts to run g09 and g16. You can submit calculation with either g09 or g16 running the following line:
```bash
qg09sub name hh:mm:ss
qg16sub name hh:mm:ss
```
were _name_ correspond to the name.com file. If time is not mentioned, then 1:00:00 (1hr) is included by default. In case you want to recover your files before the calculation finishes
```bash
job-ID  prior   name       user         state submit/start at     queue                          slots ja-task-ID 
-----------------------------------------------------------------------------------------------------------------
2298770 0.55044 a-3nh-scan user_name       r     01/26/2019 22:20:13 multiway.q@comp129                 8        
```
Once you have identified the job-ID and the node where it is running. You can copy them to your current directory:
```bash
scp user_name@compxxx:/scratch/job-ID/* .
```
Alternatively, you can use the submission file included above. For example, to submit the gaussian_test job to the queuing system run the following command

```bash
qsub run_gaussian.sh
```
