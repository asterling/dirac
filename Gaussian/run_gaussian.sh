#!/bin/csh
#
#$ -cwd
#
#$ -pe smp 2
#$ -l s_rt=360:00:00
#
source /usr/local/gaussian/scripts/g09.login
setenv ORIG $PWD
setenv SCR $TMPDIR
# Make the scratch directroy
mkdir -p $SCR
# Copy the Gaussian input (.com) file to the node
cp gaussian_test.com $SCR
#
cd $SCR
# Run gaussian
g09   <gaussian_test.com >&gaussian_test.log
#
rm -f *.rwf
# Copy only everything back to ORIG
cp * $ORIG
cd /
# Remove all the files on the node
rm -Rf $SCR
